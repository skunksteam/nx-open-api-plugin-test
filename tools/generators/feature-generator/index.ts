import {
  Tree,
  formatFiles,
  installPackagesTask,
  generateFiles,
  getProjects,
  names,
} from '@nrwl/devkit';
import { libraryGenerator } from '@nrwl/workspace/generators';
import * as path from 'path';

export default async function (host: Tree, schema: any) {
  // ------------------------------------------------------------------------------------
  // Presentation module
  // ------------------------------------------------------------------------------------
  await libraryGenerator(host, { name: `app-${schema.name}` });

  const project = getProjects(host).get(`app-${schema.name}`);
  const targetPath = path.join(project.sourceRoot, 'lib', 'toto');
  const templatePath = 'tools/generators/schematics';
  const interfaceNames = names('toto');
  const substitutions = {
    // remove __tmpl__ from file endings
    tmpl: '',
    // make the different name variants available as substitutions
    ...interfaceNames,
  };

  generateFiles(host, templatePath, targetPath, substitutions);

  // ------------------------------------------------------------------------------------
  // Store module
  // ------------------------------------------------------------------------------------
  // await libraryGenerator(host, { name: `app-${schema.name}-child-store` });

  // ------------------------------------------------------------------------------------
  // Services module
  // ------------------------------------------------------------------------------------
  // await libraryGenerator(host, { name: `app-${schema.name}-services` });
  await formatFiles(host);
  return () => {
    installPackagesTask(host);
  };
}
